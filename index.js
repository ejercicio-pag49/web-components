class MiClase extends HTMLElement {
  constructor() {
    super();
    const templateSearch = document.querySelector(".template-search");
    const tSearchInstance = templateSearch.content.cloneNode(true);
    this.attachShadow({ mode: "open" });
    this.shadowRoot.appendChild(tSearchInstance);

    let btnSearch = this.shadowRoot.querySelector(".search");
    let btnClear = this.shadowRoot.querySelector(".clear");
    let textField = this.shadowRoot.querySelector(".campoTexto");

    const areaResponse = document.querySelector(".container-response");
    const datos = document.querySelector("container-datos");
    const templateFilas = document.querySelector(".template-filas").content;
    console.log(templateFilas);

    /* btn search */
    btnSearch.addEventListener("click", e => {
      const valor = textField.value;
      fetch(`http://dummy.restapiexample.com/api/v1/employee/${valor}`)
        .then(res => {
          console.log(res);
          if (res.ok) {
            return res.json();
          } else {
            throw new Error(`Error ${res.status} ${res.statusText}`);
          }
        })
        .then(json => json.data)
        .then(data => {
          let arrNombre = data.employee_name.split(" ");
          templateFilas.querySelector(".nombre").textContent = arrNombre[0];
          templateFilas.querySelector(".apellido").textContent = arrNombre[1];
          templateFilas.querySelector(".sueldo").textContent = data.employee_salary;

          let clone = document.importNode(templateFilas, true);

          if (datos.shadowRoot.querySelector("tr") !== null) {
            let shadow = datos.shadowRoot;
            let tRow = shadow.querySelectorAll("tr");
            for (const item of tRow) {
              item.remove();
            }
          }

          datos.shadowRoot.appendChild(clone);
          console.log(datos.shadowRoot.querySelector("tr"));

        })
        .catch(err => {
          alert(err);
        })
    })

    /* btn clear */
    btnClear.addEventListener("click", e => {
      let shadow = datos.shadowRoot;
      let tRow = shadow.querySelectorAll("tr");
      for (const item of tRow) {
        item.remove();
      }

    })
  }
}
customElements.define("barra-busqueda", MiClase);



class Datos extends HTMLElement {
  constructor() {
    super();
    const templateDatos = document.querySelector(".template-filas").content;
    const fragment = document.createDocumentFragment();

    this.attachShadow({ mode: "open" });
    /* let myShadowDom = templateDatos.shadowRoot; */
    let tDatosInstance = templateDatos.cloneNode(true);
    this.shadowRoot.appendChild(tDatosInstance);

    let style = document.createElement("style");
    style.textContent = `
         :host{
         display:block;
         }
         td{
           padding-right:4rem;
           padding-left:1rem;
         }`
      ;

    this.shadowRoot.appendChild(style);

    const http = new XMLHttpRequest();

    http.addEventListener("load", e => {
      if (e.target.status >= 200 && e.target.status < 300) {
        const response = e.target.responseText;
        let json = JSON.parse(response);
        json = json.data;
        console.log(json);

        json.forEach(el => {
          let arrNombre = el.employee_name.split(" ");
          templateDatos.querySelector(".nombre").textContent = arrNombre[0];
          templateDatos.querySelector(".apellido").textContent = arrNombre[1];
          templateDatos.querySelector(".sueldo").textContent = el.employee_salary;

          let clone = document.importNode(templateDatos, true);
          fragment.appendChild(clone);
        });
        this.shadowRoot.appendChild(fragment);


      } else {
        alert(`Error: ${e.target.status} ${e.target.statusText}\n Vuelve a intentarlo`)
      }
    });

    http.open("GET", "http://dummy.restapiexample.com/api/v1/employees");
    http.send();
  }

}

customElements.define("container-datos", Datos);

